MODULES = hamming_distance
EXTENSION = hamming_distance
DATA = hamming_distance--1.0.sql hamming_distance--unpackaged--1.0.sql
PG_CPPFLAGS = -Wno-missing-prototypes -march=native -mtune=native -O3

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
